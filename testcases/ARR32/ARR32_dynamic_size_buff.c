#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define BAD

extern void do_work(int *array, size_t size);
#ifdef BAD
void arr32_func(int size) {
  int vla[size];
  do_work(vla, size);
}
void arr32_test_def(int size) {
  int count;
  if (size < 0) {
    count = size;
  }
  if (size == 0) {
    count = size;
  }
  if (size > 0) {
  }
  if (size == -1) {
    scanf("%d", &count);
  }
  arr32_func(count);
}
#else
// Compliant Solution
enum { MAX_ARRAY = 1024 };
extern void do_work(int *array, size_t size);
  
void func(size_t size) {
  if (0 == size || SIZE_MAX / sizeof(int) < size) {
    /* Handle error */
    return;
  }
  if (size < MAX_ARRAY) {
    int vla[size];
    do_work(vla, size);
  } else {
    int *array = (int *)malloc(size * sizeof(int));
    if (array == NULL) {
      /* Handle error */
    }
    do_work(array, size);
    free(array);
  }
}
#endif



// Noncompliant Code Example (sizeof)
enum { N1 = 4096 };

void *arr32_sizeof_test(size_t n2) {
#ifndef BAD
  if (n2 > SIZE_MAX / (N1 * sizeof(int))) {
    /* Prevent sizeof wrapping */
    return NULL;
  }
#endif
  typedef int A[n2][N1];
  A *array = malloc(sizeof(A));
  if (!array) {
    /* Handle error */
    return NULL;
  }
  size_t i;
  for (i = 0; i != n2; ++i) {
    memset(array[i], 0, N1 * sizeof(int));
  }

  return array;
}
