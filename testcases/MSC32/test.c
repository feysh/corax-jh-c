#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define BAD

void func(void) {
#ifdef BAD
  srandom(0);
#else
  struct timespec ts;
  if (timespec_get(&ts, TIME_UTC) == 0) {
    /* Handle error */
    return;
  }
  srandom(ts.tv_nsec ^ ts.tv_sec);
#endif
  unsigned int i;
  for (i = 0; i < 10; ++i) {
    /* Generates different sequences at different runs */
    printf("%ld, ", random());
  }
}
