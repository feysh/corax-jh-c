#include <stdint.h>
#include <stdlib.h>

void alloc_intbuffer(size_t len) {
  long *p;
  size_t sz = sizeof(long);
  p = (long *)malloc(len * sz);
  if (p == NULL) {
    /* Handle error */
    return;
  }
  free(p);
}


void mem35_test(size_t len) {
  if (len == 0 || len > 22) {
#ifndef BAD
    /* Handle overflow */
    return;
#endif
  }
  alloc_intbuffer(len);
}



